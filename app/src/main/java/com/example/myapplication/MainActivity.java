package com.example.myapplication;


import androidx.appcompat.app.AppCompatActivity;


import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0; //счетчик
    private Integer layout_width = 0; //значение размера
    private Integer new_width = 0; //новое значение размера


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * при нажатии на клавишу увеличивает число counter на 1
     * и расширяет границы заднего фона в зависимости от размеров занимаемых числом
     */
    public void onClickBtnAddStudents(View view) {
        counter++; //счетчик+1
        TextView counterView = (TextView) findViewById(R.id.txt_counter);

        counterView.setText(counter.toString());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            counterView.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        }

        ViewGroup.LayoutParams layoutParams = counterView.getLayoutParams(); //создаются параметры для новых значений размера

        layout_width = counter.toString().length(); //вычисляется размер тектового поля

        if (layout_width > new_width) {
            new_width = layout_width;
            layoutParams.width = layout_width * 100; //увеличение ширины
            counterView.setLayoutParams(layoutParams);
        }
    }

    /**
     * Сохраняет значение счетчика при смене ориентации
     */
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("count", counter);
        super.onSaveInstanceState(outState);
    }

    /**
     * Восстанавливает значение счетчика при смене ориентации
     */
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        counter = savedInstanceState.getInt("count");
        super.onRestoreInstanceState(savedInstanceState);
    }
}